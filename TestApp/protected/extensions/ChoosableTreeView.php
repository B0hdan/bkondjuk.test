<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of EditableTreeView
 *
 * @author bohdan
 */
class ChoosableTreeView extends CTreeView {
    
    public $selected = array();
    
    
    /**
     * Initializes the widget.
     * This method registers all needed client scripts and renders
     * the tree view content.
     */
    public function init()
    {
            if(isset($this->htmlOptions['id']))
                    $id=$this->htmlOptions['id'];
            else
                    $id=$this->htmlOptions['id']=$this->getId();
            if($this->url!==null)
                    $this->url=CHtml::normalizeUrl($this->url);
            $cs=Yii::app()->getClientScript();
            $cs->registerCoreScript('treeview');
            $options=$this->getClientOptions();
            $options=$options===array()?'{}' : CJavaScript::encode($options);
            $cs->registerScript('Yii.CTreeView#'.$id,"jQuery(\"#{$id}\").treeview($options);");
            if($this->cssFile===null)
                    $cs->registerCssFile($cs->getCoreScriptUrl().'/treeview/jquery.treeview.css');
            elseif($this->cssFile!==false)
                    $cs->registerCssFile($this->cssFile);

            
            echo CHtml::tag('ul',$this->htmlOptions,false,false)."\n";
            echo self::saveDataAsHtml($this->data, $this->selected);
    }
    
    public static function saveDataAsHtml($data, $selected=array()) {
        $html='';
        if(is_array($data))
        {
                foreach($data as $node)
                {
                        if(!isset($node['text']))
                                continue;

                        if(isset($node['expanded']))
                                $css=$node['expanded'] ? 'open' : 'closed';
                        else
                                $css='';

                        if(isset($node['hasChildren']) && $node['hasChildren'])
                        {
                                if($css!=='')
                                        $css.=' ';
                                $css.='hasChildren';
                        }

                        $options=isset($node['htmlOptions']) ? $node['htmlOptions'] : array();
                        if($css!=='')
                        {
                                if(isset($options['class']))
                                        $options['class'].=' '.$css;
                                else
                                        $options['class']=$css;
                        }

                        if(isset($node['id']))
                                $options['id']=$node['id'];

                        //$html .= self::getCheckBox($node);
                        
                        $html .= 
                            CHtml::tag('li',$options, 
                                self::getCheckBox($node, $selected) . $node['text'], false);
                        
                        //$html .= self::getCheckBox($node);
                        
                        
                        if(!empty($node['children']))
                        {
                                $html.="\n<ul>\n";
                                $html.=self::saveDataAsHtml($node['children'], $selected);
                                $html.="</ul>\n";
                        }
                        $html.=CHtml::closeTag('li')."\n";
                }
        }
        return $html;
    }
    
    
    /**
     * 
     * @param type $node
     * @return type
     */
    private static function getCheckBox($node, $selected) {
                
        return
              '<span style="margin: 0 10px 0 3px;">'
                . '<input style="margin:0;" type="checkbox" '
                  . 'value="'. $node['id'] . '" '
                  . (in_array($node['id'], $selected) ? 'checked=""' : '')
                  . 'name="Article[categories][]">'
            . '</span>';
    }

}
