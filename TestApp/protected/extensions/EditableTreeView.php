<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of EditableTreeView
 *
 * @author bohdan
 */
class EditableTreeView extends CTreeView {
    
    /**
     * Initializes the widget.
     * This method registers all needed client scripts and renders
     * the tree view content.
     */
    public function init()
    {
            if(isset($this->htmlOptions['id']))
                    $id=$this->htmlOptions['id'];
            else
                    $id=$this->htmlOptions['id']=$this->getId();
            if($this->url!==null)
                    $this->url=CHtml::normalizeUrl($this->url);
            $cs=Yii::app()->getClientScript();
            $cs->registerCoreScript('treeview');
            $options=$this->getClientOptions();
            $options=$options===array()?'{}' : CJavaScript::encode($options);
            $cs->registerScript('Yii.CTreeView#'.$id,"jQuery(\"#{$id}\").treeview($options);");
            if($this->cssFile===null)
                    $cs->registerCssFile($cs->getCoreScriptUrl().'/treeview/jquery.treeview.css');
            elseif($this->cssFile!==false)
                    $cs->registerCssFile($this->cssFile);

            echo CHtml::tag('ul',$this->htmlOptions,false,false)."\n";
            echo self::saveDataAsHtml($this->data);
    }
    
    public static function saveDataAsHtml($data) {
        $html='';
        if(is_array($data))
        {
                foreach($data as $node)
                {
                        if(!isset($node['text']))
                                continue;

                        if(isset($node['expanded']))
                                $css=$node['expanded'] ? 'open' : 'closed';
                        else
                                $css='';

                        if(isset($node['hasChildren']) && $node['hasChildren'])
                        {
                                if($css!=='')
                                        $css.=' ';
                                $css.='hasChildren';
                        }

                        $options=isset($node['htmlOptions']) ? $node['htmlOptions'] : array();
                        if($css!=='')
                        {
                                if(isset($options['class']))
                                        $options['class'].=' '.$css;
                                else
                                        $options['class']=$css;
                        }

                        if(isset($node['id']))
                                $options['id']=$node['id'];

                        $html .= CHtml::tag('li',$options,$node['text'],false);
                        
                        
                        
                        $html .= self::getAddButton($node['id']);
                        $html .= self::getEditButton($node['id']);
                        $html .= self::getDeleteButton($node['id']);
                        
                        
                        if(!empty($node['children']))
                        {
                                $html.="\n<ul>\n";
                                $html.=self::saveDataAsHtml($node['children']);
                                $html.="</ul>\n";
                        }
                        $html.=CHtml::closeTag('li')."\n";
                }
        }
        return $html;
    }
    
    /**
     * 
     * @param type $id
     * @return type
     */
    private static function getAddButton($id) {
        return CHtml::link(
            CHtml::image('/TestApp/images/icons/add.png','Create sub category'), 
            array('category/create', 'pid'=>$id), array('title'=>'Create sub category')
        );
    }
    
    
    /**
     * 
     * @param type $id
     * @return type
     */
    private static function getEditButton($id) {
        return  CHtml::link(
                    CHtml::image('/TestApp/images/icons/edit.png','Edit'),
                    array('category/update', 'id'=>$id), array('title'=>'Edit')
                );
    }
    
    
    /**
     * 
     * @param type $id
     * @return type
     */
    private static function getDeleteButton($id) {
        return CHtml::ajaxLink(
            CHtml::image('/TestApp/images/icons/delete.png','delete'),
            Yii::app()->createUrl('category/delete', array('id'=>$id)),
            array(
                    'type'=>'POST',
                    'success'=>'function(data) {
                                top.location.href="'.Yii::app()->createUrl('category/index').'"; 
                            }',
                    ),
            array(
                    'href'=>Yii::app()->createUrl('category/delete', array('id'=>$id)),
                    'confirm' => 'Are you sure you want to delete?',
                    'title'=>'Deletion',
                    )
            );
    }
}
