<?php
/* @var $this CategoryController */
/* @var $model Category */

$this->breadcrumbs=array(
	'Categories'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Category', 'url'=>array('index')),
	array('label'=>'Manage Category', 'url'=>array('admin')),
);
?>

<h1>Create Category</h1>
<div class="form">

    <form id="category-form"
        action="/TestApp/index.php?r=category/create&amp;pid=<?php echo $data->pid; ?>" method="post">
        <p class="note">Fields with <span class="required">*</span> are required.</p>

        <?php if (isset($data->pid)): ?>
        <input type="hidden" name="Category[pid]" value="<?php echo $data->pid; ?>">

        <div class="row">
            <label for="Category_pid">Parent Category</label>
            <input  type="text" size="60" maxlength="128" disabled=""
                   value="<?php echo $data->parent_name; ?>">
        </div>
        <?php endif; ?>
        
        <div class="row">
            <label for="Category_name" class="required">Name <span class="required">*</span></label>
            <input size="60" maxlength="128" name="Category[name]" id="Category_name" type="text">
        </div>

        <div class="row buttons">
            <input type="submit" name="yt0" value="Create">
        </div>

    </form>
</div>


<?php #$this->renderPartial('_form', array('model'=>$model)); ?>

