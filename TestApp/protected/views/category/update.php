<?php
/* @var $this CategoryController */
/* @var $model Category */

$this->breadcrumbs=array(
	'Categories'=>array('index'),
	$data->name=>array('view','id'=>$data->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Category', 'url'=>array('index')),
	array('label'=>'Create Category', 'url'=>array('create')),
	array('label'=>'View Category', 'url'=>array('view', 'id'=>$data->id)),
	array('label'=>'Manage Category', 'url'=>array('admin')),
);
?>

<h1>Update Category <?php echo $data->name; ?></h1>

<div class="form">

    <form id="category-form" 
        action="/TestApp/index.php?r=category/update&amp;id=<?php echo $data->id; ?>" method="post">
        <p class="note">Fields with <span class="required">*</span> are required.</p>

        <input type="hidden" value="<?php echo $data->id; ?>" name="Category[id]">
        <input type="hidden" value="<?php echo $data->pid; ?>" name="Category[pid]">
        
        <?php if (isset($data->parent_name)): ?>
            <div class="row">
                <label for="Category_pid">Parent Category</label>
                <input  type="text" size="60" maxlength="128" disabled=""
                       value="<?php echo $data->parent_name; ?>">
            </div>
        <?php endif; ?>
        
        <div class="row">
            <label for="Category_name" class="required">Name <span class="required">*</span></label>
            <input size="60" maxlength="128" name="Category[name]" id="Category_name" type="text"
                   value="<?php echo $data->name; ?>">
        </div>

        <div class="row buttons">
            <input type="submit" name="yt0" value="Update">
        </div>

    </form>
    
</div>

<?php #$this->renderPartial('_form', array('model'=>$model)); ?>