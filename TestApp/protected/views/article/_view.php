<?php
/* @var $this ArticleController */
/* @var $data Article */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('title')); ?>:</b>
	<?php echo CHtml::encode($data->title); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('text')); ?>:</b>
	<?php echo CHtml::encode($data->text); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fixed')); ?>:</b>
	<?php echo CHtml::encode($data->fixed); ?>
	<br />
    
    
    
    <b><?php echo CHtml::encode($data->getAttributeLabel('Categories')); ?>:</b>
	<?php
        $categories = array();
        foreach($data->categories as $category) {
           $categories[] = $category->name; 
        };
        echo CHtml::encode(join(', ', $categories));
    ?>
	<br />


</div>