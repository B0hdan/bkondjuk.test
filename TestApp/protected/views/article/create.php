<?php
/* @var $this ArticleController */
/* @var $model Article */

$this->breadcrumbs=array(
	'Articles'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Article', 'url'=>array('index')),
	array('label'=>'Manage Article', 'url'=>array('admin')),
);
?>

<h1>Create Article</h1>
<div class="form">

    <form id="article-form" action="/TestApp/index.php?r=article/create" method="post">
        <p class="note">Fields with <span class="required">*</span> are required.</p>

        <div style="float:left;">
            <div class="row">
                <label for="Article_title" class="required">Title <span class="required">*</span></label>
                <input size="60" maxlength="128" name="Article[title]" id="Article_title" type="text">
            </div>

            <div class="row">
                <label for="Article_text">Text</label>
                <textarea rows="6" cols="50" name="Article[text]" id="Article_text"></textarea>
            </div>

            <div class="row">
                <label for="Article_fixed">Fixed</label>
                <input name="Article[fixed]" id="Article_fixed" type="checkbox" value="1">
            </div>

        </div>
        <div style="float:right;">
            <span style="font-weight: bold; font-size: 0.9em; display: block;">
                Categories
            </span>
            <?php 
                $this->widget('application.extensions.ChoosableTreeView', array('data' => $categoryTree));
            ?>
        </div>
        
       
        <div style="clear: both;"></div>
        
        <div class="row buttons">
            <input type="submit" name="yt0" value="Create">
        </div>
        

    </form>
</div>

<?php #$this->renderPartial('_form', array('model'=>$model)); ?>

<?php #$this->widget('CTreeView', array('data' => $categoryTree));?>