<?php
/* @var $this ArticleController */
/* @var $model Article */

$this->breadcrumbs=array(
	'Articles'=>array('index'),
	$model->title=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Article', 'url'=>array('index')),
	array('label'=>'Create Article', 'url'=>array('create')),
	array('label'=>'View Article', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage Article', 'url'=>array('admin')),
);
?>

<h1>Update Article <?php echo $model->id; ?></h1>

<div class="form">

    <form id="article-form" action="/TestApp/index.php?r=article/update&id=<?php echo $model->id?>" method="post">
        <p class="note">Fields with <span class="required">*</span> are required.</p>

        <div style="float:left;">
            <div class="row">
                <label for="Article_title" class="required">Title <span class="required">*</span></label>
                <input size="60" maxlength="128" name="Article[title]" 
                       id="Article_title" type="text"
                       value="<?php echo $model->title;?>">
            </div>

            <div class="row">
                <label for="Article_text">Text</label>
                <textarea rows="6" cols="50" name="Article[text]" id="Article_text"
                    ><?php echo $model->text; ?></textarea>
            </div>

            <div class="row">
                <label for="Article_fixed">Fixed</label>
                <input name="Article[fixed]" id="Article_fixed" type="checkbox" value="1"
                <?php echo $model->fixed ? 'checked=""' : ''?>       
                >
            </div>

        </div>
        <div style="float:right;">
            <span style="font-weight: bold; font-size: 0.9em; display: block;">
                Categories
            </span>
            <?php 
                $this->widget('application.extensions.ChoosableTreeView',
                    array(
                        'data' => $categoryTree,
                        'selected' => $selectedCategories,
                    )
                );
            ?>
        </div>
        
       
        <div style="clear: both;"></div>
        
        <div class="row buttons">
            <input type="submit" name="yt0" value="Update">
        </div>
        

    </form>
</div>

<?php #$this->renderPartial('_form', array('model'=>$model)); ?>