<?php
/* @var $this SiteController */

$this->pageTitle=Yii::app()->name;
?>


<?php Yii::app()->clientScript->registerScriptFile("protected/views/site/js/index.js"); ?>

<div style="width: 600px; float: left;">
    <h2 style="text-align: center;">Articles</h2>
    <div style="margin-bottom: 25px; text-align: right;">
<?php
    echo '<label for="sorting-by">Sort By:</label>';

    echo CHtml::dropDownList('srot_by','',
        array(
            "id"    => 'Id',
            "title" => 'Title',
            "text"  => 'Text',
//            "fixed" => 'Fixed',
        ),
        array(
            'id'       => 'sort-by',
            'style'     => "margin: 0 5px;",
        )
    );
 
    echo '<label for="sorting-type">Sorting Type:</label>';
 
    echo CHtml::dropDownList('sort', '', 
        array(
            'default'     => 'Default',
            'randomize'   => 'Randomize',
            'reverse'     => 'Reverse',
            'random_rot'  => 'Random rotate',
        ),
        array(
            'id'        => 'sorting-type',
            'style'     => "margin: 0 5px;",
        )
    );
?>
    </div>
    
<?php 
    $this->widget('zii.widgets.CListView', array(
        'dataProvider' => $articleDataProvider,
        'itemView' => '_view',
        'id' => 'ajaxListView',
    ));
?>
</div>


<div style="width: 300px; float: right;">
    
    <h2 style="text-align: center;">Categories</h2>
    <div style="float: right;">
        <?php 
            $this->widget('application.extensions.ChoosableTreeView', array(
                'data' => $categoryTree, 'id' => 'categories-list'));
        ?>
    </div>
</div>