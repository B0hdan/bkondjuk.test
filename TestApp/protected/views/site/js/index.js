$(function(){
  var
    $sortBy         = $('#sort-by'),
    $sortType       = $('#sorting-type'),
    $categoriesList = $('#categories-list');
  
  var updateArticles = function() {
    var
      url,
      sort_by,
      sorting_type,
      categoriesIds = [];
    
    sort_by      = $sortBy.val();
    sorting_type = $sortType.val();
    
    $.each($categoriesList.find('input').filter(':checked'), function(index, input) {
      categoriesIds.push($(input).val());
    });
    
    categoriesIds.join(',');
    
    url = 'index.php?sorting=' + sorting_type 
        + '&sort_by=' + sort_by
        + (categoriesIds.length ? '&categories=(' + categoriesIds.join(',') + ')' : ''); 
    
    $.fn.yiiListView.update("ajaxListView", {"url" : url });
    
  };
  
  
  $categoriesList.find('input').on('change', function() {
    updateArticles();
  });
  
  $sortBy.on('change', function() {
    updateArticles();
  });
  
  $sortType.on('change', function() {
    updateArticles();
  });
  
  
});