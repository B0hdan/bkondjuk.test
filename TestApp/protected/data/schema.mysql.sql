CREATE DATABASE `job` CHARACTER SET utf8 COLLATE utf8_general_ci;

use job;

CREATE TABLE user (
    id INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
    username VARCHAR(128) NOT NULL,
    password VARCHAR(128) NOT NULL,
    email VARCHAR(128) NOT NULL
);


INSERT INTO user (username, password, email) VALUES ('test1', 'pass1', 'test1@example.com');
INSERT INTO user (username, password, email) VALUES ('test2', 'pass2', 'test2@example.com');
INSERT INTO user (username, password, email) VALUES ('test3', 'pass3', 'test3@example.com');


/* Category Table */


CREATE TABLE category (
    id INTEGER NOT NULL AUTO_INCREMENT,
    pid  INTEGER,
    name VARCHAR(128) NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (pid) REFERENCES category(id) ON DELETE CASCADE
);

INSERT INTO category ( name ) VALUES 
("animal"),
("vegetable"),
("mineral");

INSERT INTO category ( id, name, pid ) VALUES

(4, "doggie", 1),
(5, "kittie", 1),
(6, "horsie", 1),
(7, "gerbil", 1),
(8, "birdie", 1),
(9, "carrot", 2),
(10, "tomato", 2),
(11, "potato", 2),
(12, "celery", 2),
(13, "rutabaga", 2),
(14, "quartz", 3),
(15, "feldspar", 3),
(16, "silica", 3),
(17, "gypsum", 3),
(18, "hunting", 4),
(19, "companion", 4),
(20, "herding", 4),
(21, "setter", 18),
(22, "pointer", 18),
(23, "terrier", 18),
(24, "poodle", 19),
(25, "chihuahua", 19),
(26, "shepherd", 20),
(27, "collie", 20);


CREATE TABLE article (
    id INTEGER NOT NULL AUTO_INCREMENT,
    title varchar(128) NOT NULL,
    text text,
    fixed boolean default 0 NOT NULL,
    PRIMARY KEY(id)
);

CREATE TABLE article_categories (
    aid INTEGER NOT NULL,
    cid INTEGER NOT NULL,
    PRIMARY KEY (aid, cid),
    FOREIGN KEY (aid) REFERENCES article(id),
    FOREIGN KEY (cid) REFERENCES category(id)
);


INSERT INTO article (title, fixed) VALUES
('A', 0),
('B', 1),
('C', 0),
('D', 0),
('E', 1),
('F', 0),
('G', 0),
('H', 0)
;


INSERT INTO article_categories (aid, cid) VALUES
(1, 1),
(1, 7),
(2, 3),
(3, 9),
(4, 2),
(5, 19),
(6, 10),
(7, 1),
(8, 1);