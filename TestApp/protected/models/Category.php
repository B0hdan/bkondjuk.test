<?php

/**
 * This is the model class for table "category".
 *
 * The followings are the available columns in table 'category':
 * @property integer $id
 * @property integer $pid
 * @property string $name
 *
 * The followings are the available model relations:
 * @property Category $p
 * @property Category[] $categories
 */
class Category extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'category';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name', 'required'),
			array('pid', 'numerical', 'integerOnly'=>true),
			array('name', 'length', 'max'=>128),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, pid, name', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'p' => array(self::BELONGS_TO, 'Category', 'pid'),
			'categories' => array(self::HAS_MANY, 'Category', 'pid'),
            'articles' => array(self::MANY_MANY, 
                'Article', 'article_categories(aid, cid)'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'pid' => 'Pid',
			'name' => 'Name',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('pid',$this->pid);
		$criteria->compare('name',$this->name,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        
        
        /**
         * Get categories hierarchy
         * 
         * @return Array List of categories hierarchy
         */
        public function getCategoriesHierarchy($expanded = FALSE)
        {
            $refs = array();
            $list = array();
            
            $categories = Yii::app()->db->createCommand()
                        ->select('*')
                        ->from('category')
                        ->order('pid ASC')
                        ->queryAll();
            
            foreach($categories as $data) {
                $thisref = &$refs[ $data['id'] ];
                $thisref['id'] = $data['id'];
                $thisref['pid'] = $data['pid'];
                $thisref['text'] = $data['name'];
                $thisref['expanded'] = $expanded;
                
                if ( is_null($data['pid']) ) {                    
                    $list[ $data['id'] ] = &$thisref;
                } else {
                    $refs[ $data['pid'] ]['children'][ $data['id'] ] = &$thisref;
                }
            }
            
            
            return $list;

        }

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Category the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
