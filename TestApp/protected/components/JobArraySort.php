<?php
/**
 * JobArraySort : Helper
 *
 * @author bohdan
 */
class JobArraySort {
    /**
     * Shuffle the articles by Fisher and Yates algorithm
     * in view of "fixed" items
     * 
     *  for i from 0 to n − 1 do
     *    j ← random integer with i ≤ j < n
     *    exchange a[j] and a[i]
     * 
     * @param CActiveDataProvider $dataProvider
     */
    public static function shuffle(CActiveDataProvider $dataProvider)
    {
        $shuffled = $dataProvider->data;

        $size = count($shuffled);
        
        for ($i = 0; $i < $size; $i++) {
            $j = rand($i, $size-1);


            if (!($shuffled[$i]->fixed || $shuffled[$j]->fixed)) {
                self::swap($shuffled, $i, $j);
            }
        }

        $dataProvider->data = $shuffled;            
    }
    
    
    /**
     * Reverse array in view of "fixed" items
     * 
     * @param CActiveDataProvider $dataProvider
     */
    public static function reverse(CActiveDataProvider $dataProvider)
    {
        $original = $dataProvider->data;   
        
        $reversed = array_reverse($original);

        self::replaceOriginal($original, $reversed);

        $dataProvider->data = $original;
    }
    
    /**
     * Random rotate in view of "fixed" items
     * 
     * @param CActiveDataProvider $dataProvider
     */
    public static function randomRotate(CActiveDataProvider $dataProvider)
    {
        $original = $dataProvider->data;
        
        $rotated = array();        
        $count   = count($original);
        $key     = rand(0, count($original)-1);

        for ($i=0; $i < $count; $i++, $key++) {
            $rotated[] = $original[ $key % $count ];
        }

        self::replaceOriginal($original, $rotated);

                
        $dataProvider->data = $original;
        
    }
    
    /**
     * Replace the original array by the modified array
     * in view of "fixed" items.
     * 
     * @param array $original
     * @param array $modified
     */
    private static function replaceOriginal(Array &$original, Array $modified) {        
        foreach($original as $i => $value) {
            if ($value['fixed']) {
                continue;
            }   

            foreach($modified as $key => $rvalue) {
                if ($rvalue['fixed']) {
                   continue;
                }   
                $original[$i] = $rvalue;
                unset($modified[$key]);
                break;
            }   
        }
    }
    
    
    /**
     * Swap elements of array
     * 
     * @param array $arr
     * @param integer $i
     * @param integer $j
     */
    private static function swap(Array &$arr, $i, $j) {
        $tmp = $arr[$i];
        $arr[$i] = $arr[$j];
        $arr[$j] = $tmp;
    }
}
